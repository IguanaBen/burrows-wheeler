#ifndef SUFFIXARRAY_H
#define SUFFIXARRAY_H

#include <algorithm>
#include <list>
#include <vector>

using namespace std;

//letters from 0 to text.length()-1
vector<unsigned int> build_suffix_array(vector<unsigned int> &text);

#endif
