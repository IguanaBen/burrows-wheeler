#ifndef SUFFIXTREE_H
#define SUFFIXTREE_H

#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <unordered_map>

using namespace std;


struct suffix_tree_node
{
	// list< pair<int, suffix_tree_node*> > children;
	map<int, suffix_tree_node*> children;
	suffix_tree_node* slink;	//leafs may not have slinks
	unsigned int text_ind; //starting position of substring represented
						   // by this node
	unsigned int edge_length;   // length of the edge=(parent, this_node)
};

class suffix_tree
{
	public:
		const unsigned int inf = 2147483647;
		vector<int> text;
		suffix_tree_node* root;
		void traverse(suffix_tree_node* node, vector<unsigned int> &vec,
							unsigned int &cnt, unsigned int depth);
		void del_tree(suffix_tree_node* node);
		// suffix_tree_node* find_active_node(suffix_tree_node *active_node, 
		// 					unsigned int active_len, int active_char);
		// pair<suffix_tree_node<charType>*, int> find_slink(pair<suffix_tree_node<charType>*, int> node);

	public:
		suffix_tree(vector<int> &_text);
		~suffix_tree();
		vector<unsigned int> make_array();
};

#endif
