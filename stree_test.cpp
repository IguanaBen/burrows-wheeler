/*
Michał Gańczorz 2015
Compression based on Burrows-Wheeler transformation
*/

// #include <dirent.h> 
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include <list>
#include <set>
#include <map>

using namespace std;

struct suffix_tree_node
{
	// list< pair<int, suffix_tree_node*> > children;
	map<int, suffix_tree_node*> children;
	suffix_tree_node* slink;	//leafs may not have slinks
	unsigned int text_ind; //starting position of substring represented
						   // by this node
	unsigned int edge_length;   // length of the edge=(parent, this_node)
};

class suffix_tree
{
	public:
		const unsigned int inf = 2147483647;
		vector<int> text;
		suffix_tree_node* root;
		void traverse(suffix_tree_node* node, vector<unsigned int> &vec,
							unsigned int &cnt, unsigned int depth);
		// suffix_tree_node* find_active_node(suffix_tree_node *active_node, 
		// 					unsigned int active_len, int active_char);
		// pair<suffix_tree_node<charType>*, int> find_slink(pair<suffix_tree_node<charType>*, int> node);

	public:
		suffix_tree(vector<int> &_text);
		vector<unsigned int> make_array();
};

suffix_tree::suffix_tree(vector<int> &_text)
{
	text = _text;
	root = new suffix_tree_node;
	root->slink = root;

	suffix_tree_node* active_node = root;
	unsigned int active_len = 0;
	int active_char = 0;
	int t_index = 0;

	suffix_tree_node* last_added = NULL;
	// cout<<root<<"\n";
	for(unsigned int i=0; i<text.size(); ++i)
	{
		// std::cout<<"adding "<<i<<"-th character "<<char(text[i])<<", active_node: "
		// 		 <<active_node<<" "<<active_len<<" "<<char(active_char)<<"\n";
		bool cont = true;
		do
		{	
			// std::cout<<"adding "<<i<<"-th character "<<char(text[i])<<", active_node: "
				 // <<active_node<<" "<<active_len<<" "<<char(active_char);
				 // if(active_node->children.find(active_char) !=  active_node->children.end())
				 	// cout<<" "<<active_node->children[active_char]->edge_length;
				 // else
				 	// cout<<"\n";
			// active_node = find_active_node(active_node, active_len, active_char, t_index);

			if(active_len == 0)
			{
				if(active_node->children[text[i]] == 0)
				{
					// cout<<"adding leaf\n";
					suffix_tree_node* new_leaf = new suffix_tree_node;
					new_leaf->text_ind = i;
					new_leaf->edge_length = inf;
					active_node->children[text[i]] = new_leaf;

					active_char = text[i];

					if(last_added != NULL)
					{
						// cout<<"setting\n";
						// cout<<last_added<<"\n";
						// cout<<active_node<<"\n";
						last_added->slink = active_node;
					}

					if(active_node == root)
						cont = false;
					else
						active_node = active_node->slink;

					last_added = NULL;
				}
				else
				{
					// cout<<"step\n";
					active_len++;

					if(last_added != NULL)
						last_added->slink = active_node;

					active_char = text[i];

					if(active_node->children[text[i]]->edge_length == 1)
					{
						active_node = active_node->children[text[i]];
						active_len = 0;
					}

					last_added = NULL;
					cont = false;
				}
			}
			else
			{

				//tu tez trzeba aktualizowac last_added bra... 
				unsigned int index = active_node->children[active_char]->text_ind + active_len;
				if( text[i] == text[index])
				{
					active_len++;

					if(active_node->children[active_char]->edge_length == active_len)
					{
						active_node = active_node->children[active_char];
						active_len = 0;
						active_char = text[i];

						if(last_added != NULL)
							last_added->slink = active_node;	//innego przypadku nie moze byc gdy text[i] == text[index]
					}

					last_added = NULL;
					cont = false;
				}
				else
				{
					// cout<<"new node\n";

					suffix_tree_node* child = active_node->children[active_char];
					suffix_tree_node* new_leaf = new suffix_tree_node;
					suffix_tree_node* new_node = new suffix_tree_node;

					new_leaf->text_ind = i;
					new_leaf->edge_length = inf;

					new_node->text_ind = child->text_ind;
					new_node->edge_length = active_len;

					child->text_ind = child->text_ind + active_len;
					if(child->edge_length != inf)
						child->edge_length -= active_len;

					active_node->children[active_char] = new_node;

					new_node->children[text[i]] =  new_leaf;
					new_node->children[text[index]] = child;

					if(last_added != NULL)
						last_added->slink = new_node;


					last_added = new_node;

					unsigned int t_index;
					if(active_node == root)
					{
						// cout<<"this\n";
						if(active_len == 1)
						{
							new_node->slink = root;
							last_added = NULL;
						}

						active_len--;

						// if(active_len != 0)
						active_char = text[ new_node->text_ind + 1];
						
						t_index = new_node->text_ind + 1;
						// cout<<"achange: "<<char(active_char)<<" "<<active_len<<"\n";
					}
					else
					{
						// cout<<"WARNING\nWARNING\nWARNING\n";	
						last_added = new_node;
						t_index = active_node->children[active_char]->text_ind;
					}

					//??
					active_node = active_node->slink;
					// cout<<active_node->children[active_char]->edge_length<<"\n";
					while(active_len > 0 && active_node->children[active_char]->edge_length <= active_len)
					{
						// cout<<"changing\n";
						unsigned int e_length = active_node->children[active_char]->edge_length;
						active_len -= e_length;
						active_node = active_node->children[active_char];

						if(active_len != 0)
							active_char = text[t_index + e_length];
					}
					// cout<<"Overdiri\n";
					
					// cout<<active_len<<"\n";

					// if(active_node == )

				}
			}

		}while(cont);

		// cout<<"\n\n";
	}
}

vector<unsigned int> suffix_tree::make_array()
{
	vector<unsigned int> result;
	result.resize(text.size());

	unsigned int cnt = 0;
	traverse(root, result, cnt, 0);

	return result;
}

void suffix_tree::traverse(suffix_tree_node* node, vector<unsigned int> &vec,
							unsigned int &cnt, unsigned int depth)
{
	if(node->children.size() == 0)
		vec[cnt++] = text.size() - depth;
	else
	{
		  map<int, suffix_tree_node*>::iterator it;
		  for(it = node->children.begin(); it != node->children.end(); ++it)
		  {
		  	if(it->second->edge_length != inf)
		  	{
				// cout<<"depth: "<<depth
				// 	<<" fist letter: "<<char(it->first)<<" \n";
		  		traverse(it->second, vec, cnt, depth + it->second->edge_length);
		  	}
		  	else
		  	{
				// cout<<"depth: "<<depth
					// <<" fist letter: "<<char(it->first)<<" \n";
		  		traverse(it->second, vec, cnt, depth + (text.size() - it->second->text_ind));
		  	}
		  }
	}

	// cout<<"UP\n";
}


int main( int argc, char** argv)
{

	string str;
	cin>>str;
	std::vector<int> vv(str.begin(), str.end());
	vv.push_back(0);

	suffix_tree tree(vv);
	vector<unsigned int> sa = tree.make_array();
	cout<<"\n";
	for(int i=1; i<sa.size(); ++i)
		cout<<sa[i]<<"\n";

	return 0;
}