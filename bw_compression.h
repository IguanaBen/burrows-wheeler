#ifndef BW_COMPRESSION_H
#define BW_COMPRESSION_H

#include <vector>
#include <string>
#include <map>
#include <algorithm>


using namespace std;

class chunk
{
	private:
		wstring charset;
		unsigned int uncompressed_length;
		unsigned int bwt_index;

	public:
		void set_charset(wstring ch){ charset = ch; };
		void set_uncompressed_length(unsigned int len){ uncompressed_length = len; };
		void set_bwt_index(unsigned int bwt_ind){ bwt_index = bwt_ind; };
		vector<bool> huffman_out;
		map<unsigned int, string> huffman_dict;
		void save(ofstream &file);

};

pair<vector<int>, wstring> rescale(wstring &text);
wstring r_rescale(vector<int> &text, wstring &charset);

//method: 0-suffix tree; 1-suffix array, both O(n)
pair<vector<int>, unsigned int> burrows_wheeler_transform(vector<int> &text, bool method);
vector<int> r_burrows_wheeler_transform(vector<int> &text, unsigned int index);

vector<unsigned int> mtf(vector<int> &text, unsigned int charset_size);
vector<int> r_mtf(vector<unsigned int> &text, unsigned int charset_size);

bool compress(string in_filename, string out_filename, unsigned int buffer_size, bool method);
void compress_chunk(wstring &buffer, ofstream &out_file, bool method, long &total_time);

bool decompress(string in_filename, string out_filename);
void decompress_chunk(ifstream &in_file, wofstream &out_file);

// vector<unsigned int> mtf();

#endif