#include "bw_compression.h"

#include "suffix_tree.h"
#include "suffix_array.h"

#include <set>
#include <map>
#include <locale>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <list>
#include <ctime>

#include "huffman.h"

pair<vector<int>, wstring> rescale(wstring &text)
{
	set<wchar_t> char_set;
	for(int i=0; i<text.length(); ++i)
		char_set.insert(text[i]);

	pair<vector<int>, wstring > result;
	map<wchar_t, int> chars_map;
	set<wchar_t>::iterator it;

	result.second.resize(char_set.size());

	int cnt = 0;
	for(it = char_set.begin(); it != char_set.end(); ++it)
	{
		result.second[cnt] = (*it);
		chars_map[*it] = cnt++;
	}
	result.first.resize(text.size());
	for(int i=0; i<text.size(); ++i)
		result.first[i] = chars_map[text[i]];

	return result;
}

wstring r_rescale(vector<int> &text, wstring &charset)
{
	wstring res;
	res.resize(text.size());
	for(int i=0; i<text.size(); ++i)
		res[i] = charset[text[i]];
	return res;
}

pair<vector<int>, unsigned int> burrows_wheeler_transform(vector<int> &text, bool method)
{
	pair<vector<int>, unsigned int> res;

	vector<unsigned int> s_array;
	if(!method)
	{
		vector<int> text2;
		text2.insert(text2.end(), text.begin(), text.end());
		text2.insert(text2.end(), text.begin(), text.end());
		text2.push_back(-1);
		suffix_tree tree(text2);
		s_array = tree.make_array();
	}
	else
	{
		vector<unsigned int> text2;
		text2.insert(text2.end(), text.begin(), text.end());
		text2.insert(text2.end(), text.begin(), text.end());
		s_array = build_suffix_array(text2);
	}


	res.first.resize(text.size());
	int cnt = 0;
	for(int i=0; i<s_array.size(); ++i)
	{

		if(s_array[i] < text.size())
		{
			res.first[cnt] = text[(s_array[i] + text.size()-1) % text.size()];
			if(s_array[i] == 0)
				res.second = cnt;
			cnt++;
		}
	}

	return res;
}

vector<int> r_burrows_wheeler_transform(vector<int> &text, unsigned int index)
{
	vector<int> res(text.size());

	int alphabet_size = 0;
	for(int i=0; i<text.size(); ++i)
		alphabet_size = max(alphabet_size, text[i]);
	alphabet_size++;

	vector<int> indices(alphabet_size);
	vector<int> ptr_indices(alphabet_size);

	for(int i=0; i<alphabet_size; ++i)
		ptr_indices[i] = 0;

	for(int i=0; i<text.size(); ++i)
		if(text[i] != alphabet_size-1)
			indices[text[i]+1]++;

	for(int i=1; i<alphabet_size; ++i)
		indices[i] += indices[i-1];

	vector<int> T(text.size());
	for(int i=0; i<text.size(); ++i)
	{
		T[i] = indices[text[i]] + ptr_indices[text[i]];
		ptr_indices[text[i]]++;
	}

	int j = index;
	res[text.size()-1] = text[j];
	for(int i=0; i<text.size()-1; ++i)
	{
		j = T[j];
		res[text.size()-i-2] = text[j];
	}

	return res;
}



vector<unsigned int> mtf(vector<int> &text, unsigned int charset_size)
{
	list<int> charset_list;
	for(int i=0; i<charset_size; ++i)
		charset_list.push_back(i);

	vector<unsigned int> result;
	result.resize(text.size());

	list<int>::iterator it;
	for(int i=0; i<text.size(); ++i)
	{
		it = charset_list.begin();
		int cnt = 0;
		while((*it) != text[i] && it != charset_list.end())
		{
			cnt++;
			it++;
		}

		result[i] = cnt;

		charset_list.erase(it);
		charset_list.push_front(text[i]);
	}

	return result;
}

vector<int> r_mtf(vector<unsigned int> &text, unsigned int charset_size)
{
	list<int> charset_list;
	for(int i=0; i<charset_size; ++i)
		charset_list.push_back(i);

	vector<int> result;
	result.resize(text.size());

	list<int>::iterator it;
	for(int i=0; i<text.size(); ++i)
	{
		it = charset_list.begin();
		int cnt = text[i];
		while(cnt)
		{
			cnt--;
			it++;
		}

		result[i] = (*it);

		charset_list.erase(it);
		charset_list.push_front(result[i]);
	}

	return result;
}


void compress_chunk(wstring &buffer, ofstream &out_file, bool method, long &total_time)
{
	// wcout<<buffer<<"\n";
	chunk ch;
	pair<vector<int>, wstring>  r  = rescale(buffer);
	//set charcount and charset
	ch.set_charset(r.second);
	ch.set_uncompressed_length(buffer.length());

	// for(int i=0; i<r.first.size(); ++i)
	// 	cout<<r.first[i]<<" ";
	// cout<<"\n";
	long dt = 0;
	clock_t begin  = clock();
	pair<vector<int>, unsigned int> bwt = burrows_wheeler_transform(r.first, method);
	clock_t end = clock();
	total_time += (end-begin);
	//set bwt_index
	ch.set_bwt_index(bwt.second);

	// for(int i=0; i<bwt.first.size(); ++i)
	// 	cout<<bwt.first[i]<<",";
	// cout<<"\n";

	// vector<int> rbwt = r_burrows_wheeler_transform(bwt.first, bwt.second);
	// for(int i=0; i<rbwt.size(); ++i)
	// 	cout<<rbwt[i]<<" ";
	// cout<<"\n";


	vector<unsigned int> mtf_result = mtf(bwt.first, r.second.size());
	huffman_encode( mtf_result, ch.huffman_dict, ch.huffman_out);
	ch.save(out_file);
	// for(int i=0; i<mtf_result.size(); ++i)
	// 	cout<<mtf_result[i]<<"|";
	// cout<<"\n";
	// vector<int> r_mtf_result = mtf(mtf_result, r.second.size());


	// vector<int> r_mtf_result = r_mtf(mtf_result, r.second.size());
	// for(int i=0; i<r_mtf_result.size(); ++i)
	// 	cout<<r_mtf_result[i]<<" ";
	// cout<<"\n";
	// return;

	// for(int i=0; i<bwt.first.size(); ++i)
	// 	cout<<bwt.first[i]<<" ";
	// cout<<"\n";
}

bool compress(string in_filename, string out_filename, unsigned int buffer_size, bool method)
{ 

	long total_time = 0;

	wifstream in_file(in_filename, ios::in);
	in_file.imbue(std::locale("en_US.UTF-8"));
	if (!in_file.is_open())
	{
		cerr<<"couldn't open file: "<<in_filename<<"\n";
		return false;
	}

	ofstream out_file(out_filename, ios::out | ios::binary);
	if (!out_file.is_open())
	{
		cerr<<"couldn't create file: "<<out_filename<<"\n";
		return false;
	}

	wchar_t c;
	wstring buffer;
	while(in_file.get(c))
	{
		buffer += c;
		if(buffer.length() >= buffer_size)
		{
			compress_chunk(buffer, out_file, method, total_time);
			buffer.clear();
		} 
	}

	if(buffer.length() > 0)
	{
		compress_chunk(buffer, out_file, method, total_time);
		buffer.clear();
	}

	// cout<<double(total_time)/CLOCKS_PER_SEC<<"\n";
	in_file.close();
	out_file.close();

	return true;
}

bool decompress(string in_filename, string out_filename)
{ 
	ifstream in_file(in_filename, ios::in | ios::binary);
	if (!in_file.is_open())
	{
		cerr<<"couldn't open file: "<<in_filename<<"\n";
		return false;
	}

	wofstream out_file(out_filename, ios::out);
	out_file.imbue(std::locale("en_US.UTF-8"));
	if (!out_file.is_open())
	{
		cerr<<"couldn't create file: "<<out_filename<<"\n";
		return false;
	}

	while(in_file.peek() != EOF)
		decompress_chunk(in_file, out_file);

	in_file.close();
	out_file.close();
	return true;
}

void decompress_chunk(ifstream &in_file, wofstream &out_file)
{
	unsigned int charset_length;
	in_file.read(reinterpret_cast<char*>(&charset_length), sizeof(charset_length));
	wstring charset;
	for(int i=0; i<charset_length; ++i)
	{
		unsigned int c;
		in_file.read(reinterpret_cast<char*>(&c), sizeof(c));
		wchar_t wc = c;
		charset += wc;
	}
	// cout<<"entering\n";
	// wcout<<charset<<"\n";
	unsigned int bwt_index;
	in_file.read(reinterpret_cast<char*>(&bwt_index), sizeof(bwt_index));

	vector<unsigned int> huffman_vals;
	vector<unsigned int> huffman_key_lengths;
	vector<string> huffman_keys;

	unsigned int dict_size;
	in_file.read(reinterpret_cast<char*>(&dict_size), sizeof(dict_size));

	for(int i=0; i<dict_size; ++i)
	{
		unsigned int t;
		in_file.read(reinterpret_cast<char*>(&t), sizeof(t));
		huffman_vals.push_back(t);
		in_file.read(reinterpret_cast<char*>(&t), sizeof(t));
		huffman_key_lengths.push_back(t);

		// cout<<huffman_vals[i]<<" ; "<<huffman_key_lengths[i]<<"\n";
	}

	unsigned int current_val = 0;
	unsigned int mask = (1<<31);
	unsigned int cmask = mask;
	string current_key = "";
	in_file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
	for(int i=0; i<dict_size; ++i)
	{
		 for(int j=0; j<huffman_key_lengths[i]; ++j)
		 {	
		 	if(cmask & current_val)
		 		current_key += "1";
		 	else
		 		current_key += "0";

		 	cmask >>= 1;
		 	if(cmask == 0 )
		 	{
		 		if( i + 1 != dict_size  || (i + 1 == dict_size &&  j+1 != huffman_key_lengths[i]) )
		 		{
					in_file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
		 			cmask = mask;
				}
		 	}
		 }
		 huffman_keys.push_back(current_key);
		 // cout<<huffman_vals[i]<<" "<<current_key<<"\n";
		 current_key = "";
	}

	huffman_node *root = new huffman_node();
	for(int i=0; i<dict_size; ++i)
		huffman_insert_key(root, huffman_keys[i], 0, huffman_vals[i]);

	unsigned int uncompressed_length;
	in_file.read(reinterpret_cast<char*>(&uncompressed_length), sizeof(uncompressed_length));
	huffman_node *current_node = root;

	cmask = mask;
	current_val = 0;
	vector<unsigned int> r_huffman;
	// cout<<uncompressed_length<<"\n";

	in_file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
	// cout<<current_node->left->letter<<" "<<current_node->right->letter<<" |\n";
	while(1)
	{
		bool readC;
		if(cmask & current_val)
			current_node = current_node->right;
		else
			current_node = current_node->left;

		cmask >>= 1;

		if(current_node->right == NULL)
		{
			uncompressed_length--;
			r_huffman.push_back(current_node->letter);
			current_node = root;
			if(uncompressed_length == 0)
				break;
		}

		if(cmask == 0)
		{
			cmask = mask;
			in_file.read(reinterpret_cast<char*>(&current_val), sizeof(current_val));
		}
	}

	del_huffman_tree(root);

	vector<int> rmtf_result = r_mtf(r_huffman, charset_length);
	vector<int> rbwt = r_burrows_wheeler_transform(rmtf_result, bwt_index);

	wstring uncompressed_text;
	for(int i=0; i<rbwt.size(); ++i)
		uncompressed_text += charset[rbwt[i]];
	out_file<<uncompressed_text;
	// wcout<<uncompressed_text<<"\n";
	// cout<<"fragment"<<"\n";

	// cout<<uncompressed_text.length()<<"\n";
	// wcout<<charset<<"\n";

}


void chunk::save(ofstream &file)
{
	//writing charset size and charset
	unsigned int charset_length = charset.length();

	file.write(reinterpret_cast<char*>(&charset_length), sizeof(charset_length));
	for(int i=0; i<charset.length(); ++i)
	{
		unsigned int c = charset[i];
		file.write(reinterpret_cast<char*>(&c), sizeof(c));
	}

	//writing bwt index
	file.write(reinterpret_cast<char*>(&bwt_index), sizeof(bwt_index));
	//writing huffman dictionary
	//dictionary size, value, key length, and then codes 
	unsigned int dict_size = huffman_dict.size();
	file.write(reinterpret_cast<char*>(&dict_size), sizeof(dict_size));

	map<unsigned int, string>::iterator it;
	for(it = huffman_dict.begin(); it != huffman_dict.end(); ++it)
	{
		unsigned int val = it->first;
		file.write(reinterpret_cast<char*>(&val), sizeof(val));
		unsigned int len = it->second.length();
		file.write(reinterpret_cast<char*>(&len), sizeof(len));
	}

	unsigned int current_val = 0;
	unsigned int mask = (1<<31);
	unsigned int cmask = mask;

	for(it = huffman_dict.begin(); it != huffman_dict.end(); ++it)
	{
		for(int i=0; i<it->second.length(); ++i)
		{
			if(it->second[i] == '1')
				current_val |= (cmask);
			cmask >>= 1;
			if(cmask == 0)
			{
				file.write(reinterpret_cast<char*>(&current_val), sizeof(current_val));
				current_val = 0;
				cmask = mask;
			}
		}
	}
	if(cmask != mask)
		file.write(reinterpret_cast<char*>(&current_val), sizeof(current_val));


	current_val = 0;
	cmask = mask;

	file.write(reinterpret_cast<char*>(&uncompressed_length), sizeof(uncompressed_length));
	for(int i=0; i<huffman_out.size(); ++i)
	{
		if(huffman_out[i])
			current_val |= (cmask);

		cmask >>= 1;
		if(cmask == 0)
		{
			file.write(reinterpret_cast<char*>(&current_val), sizeof(current_val));
			current_val = 0;
			cmask = mask;
		}
	}

	if(cmask != mask)
		file.write(reinterpret_cast<char*>(&current_val), sizeof(current_val));

}