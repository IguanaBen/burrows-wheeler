/*
Michał Gańczorz 2015
Compression based on Burrows-Wheeler transformation
*/

// #include <dirent.h> 
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include <list>
#include <set>
#include <map>
#include <locale>
#include <iomanip>
// #include <codecvt>

#include "suffix_tree.h"
#include "suffix_array.h"
#include "bw_compression.h"
#include "huffman.h"

using namespace std;

std::streampos fileS(std::string path)
{
	ifstream file(path, std::ios::binary | ios::in);
	// long size1 = file.tellg();

	std::streampos fsize = file.tellg();
	file.seekg(0, std::ios::end);
	fsize= file.tellg() - fsize;
	// cout<<size1<<"\n";
	file.close();
	return fsize;
}

int main( int argc, char** argv)
{

	bool ok_arg = true;

	if(argc == 4 || argc == 5)
	{
		if(string(argv[1]) == "ct" || string(argv[1]) == "ca")
			if(argc != 5) ok_arg = false;

		if(string(argv[1]) == "d")
			if(argc != 4) ok_arg = false;
	}
	else
		ok_arg = false;

	if(!ok_arg)
	{
		cout<<"Wrong number of arguments\n";
		cout<<"Use: burrows-wheeler (((ct | ca) block_size )| d ) in_file out_file\n";
		cout<<"ct - compress using suffix tree for bwt\n";
		cout<<"ca - compress using suffix array for bwt\n";
		cout<<"d - decompress\n";

		return 1;
	}
	// cout<<argv[1]<<"\n"
	if(string(argv[1]) == "ct" || string(argv[1]) == "ca")
	{

		string fnameIn = string(argv[3]);
		string fnameOut = string(argv[4]);
 
		unsigned int block_size = stoi(argv[2]);
		bool method = (string(argv[1]) == "ca");

		if(compress(fnameIn, fnameOut, block_size, method))
		{
			streampos fsize1 = fileS(fnameIn);
			streampos fsize2 = fileS(fnameOut);

			double f1 = fsize1;
			double f2 = fsize2;

			cout<<"Complete, compression ratio: "<< double(f2)/f1 <<"\n";
		}
		else
			cout<<"Error while processing\n";
	}
	else if(string(argv[1]) == "d")
	{
		string fnameIn = string(argv[2]);
		string fnameOut = string(argv[3]);

		decompress(fnameIn, fnameOut);
		cout<<"Complete\n";
	} else
	{
		cout<<"Unrecognized parmater, exiting\n";
	}


	// decompress("out", "decompressed_out.txt");

	return 0;
}