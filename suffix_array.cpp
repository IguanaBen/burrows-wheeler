#include "suffix_array.h"

#include <iostream>
#include <algorithm>

struct triple
{
	unsigned int vals[3];
	unsigned int pos;
	triple(int _f, int _s, int _t, unsigned int _pos)
	{
		vals[0]=_f; vals[1]=_s; vals[2]=_t; pos=_pos;
	}
};

struct pp
{
	unsigned int vals[2];
	unsigned int pos;
	pp(int _f, int _s, unsigned int _pos)
	{
		vals[0]=_f; vals[1]=_s; pos=_pos;
	}
};

struct cmp
{
	vector<unsigned int> *text;
	cmp(vector<unsigned int> *_text)
	{
		text = _text;
	}

    bool operator()(unsigned int a, unsigned int b)
    {
        return lexicographical_compare((*text).begin() + a, (*text).end(),
        								(*text).begin() + b, (*text).end());
    }
};


vector<unsigned int> build_suffix_array(vector<unsigned int> &text)
{
	vector<unsigned int> res(text.size());

	if(text.size() < 6) // brute force
	{
		for(int i=0; i<text.size(); ++i)
			res[i] = i;

		sort(res.begin(), res.end(), cmp(&text));
		return res;
	}


	unsigned int charset_size = 0;
	// for(int i=0; i<text.size(); ++i)
	// 	charset_size = max(text[i], charset_size);

	charset_size += text.size();
	charset_size++;

	int sub_size = 0;

	vector<triple> triples;
	for(int i=0; i + 3<=text.size(); i+=3)
	{
		triples.push_back(triple(text[i] + 1, text[i+1] + 1, text[i+2] + 1, i/3));
		sub_size += 1;
	}
	
	if(text.size() % 3 == 1)
	{
		triples.push_back(triple(text[text.size()-1] + 1, 0, 0, triples.size()));
		sub_size += 1;
	}
	
	if(text.size() % 3 == 2)
	{
		triples.push_back(triple(text[text.size()-2] + 1, text[text.size()-1] + 1, 0, triples.size()));
		sub_size += 1;
	}

	triples.push_back(triple(0,0,0, triples.size()) );

	int cnt = triples.size();
	int f_cnt = cnt-1;
	for(int i=1; i+3<=text.size(); i+=3)
	{
		triples.push_back(triple(text[i] + 1, text[i+1] + 1, text[i+2] + 1, cnt++));
		sub_size+=1;
	}
	if(text.size() % 3 == 0)
	{
		triples.push_back(triple(text[text.size()-2] + 1, text[text.size()-1] + 1, 0, triples.size()));
		sub_size += 1;
	}
	
	if(text.size() % 3 == 2)
	{
		triples.push_back(triple(text[text.size()-1] + 1, 0, 0, triples.size()));
		sub_size += 1;
	}

	// for(int i=0; i<triples.size(); ++i)
	// 	cout<<(triples[i].vals[0] )<<"|"<<(triples[i].vals[1])<<"|"<<(triples[i].vals[2])<<" ";
	// cout<<"\n";

	// for(int i=0; i<triples.size(); ++i)
	// 	cout<<triples[i].pos<<" ";
	// cout<<"\n";

	vector<vector<triple> > buckets(charset_size+1);
	for(int i=2; i>=0; --i)
	{
		for(int j=0; j<triples.size(); ++j)
			buckets[triples[j].vals[i]].push_back(triples[j]);
		
		int index = 0;
		for(int j=0; j<charset_size + 1	; ++j)
		{
			for(int k=0; k<buckets[j].size(); ++k)
				triples[index++] = buckets[j][k];
			buckets[j].clear();
		}
	}

	// for(int i=0; i<triples.size(); ++i)
	// 	cout<<(triples[i].vals[0] )<<"|"<<(triples[i].vals[1])<<"|"<<(triples[i].vals[2])<<" ";
	// cout<<"\n";

	vector<unsigned int> text_triples(triples.size());
	int rank = 0;
	for(int i=0; i<triples.size()-1; ++i)
	{
		text_triples[triples[i].pos] = rank;
		// cout<<triples[i].pos<<"|\n";
		if(triples[i].vals[0] != triples[i+1].vals[0] ||
			triples[i].vals[1] != triples[i+1].vals[1] ||
			triples[i].vals[2] != triples[i+1].vals[2])
		{
			rank++;
		}
	}
	text_triples[triples[triples.size()-1].pos] = rank;


	// for(int i=0; i<triples.size(); ++i)
		// cout<<text_triples[i]<<" ";
	// cout<<" t\n";
	// cout<<"rec\n";
	vector<unsigned int> sa_triples = build_suffix_array(text_triples);
	// cout<<"ret\n";

	// cout<<sub_size<<"\n";
	vector<unsigned int> sorted_suffixes01(sub_size);	//sa for 2/3 of suffixes
	vector<unsigned int> sorted_suffixes_ranks01(text.size());

	// for(int i=0; i<sa_triples.size(); ++i)
	// 	cout<<sa_triples[i]<<" ";
	// cout<<" s\n";

	int ind = 0;
	for(int i=0; i<sa_triples.size(); ++i)
	{
		if(sa_triples[i] < f_cnt)
		{			sorted_suffixes01[ind] = sa_triples[i]*3;
			sorted_suffixes_ranks01[sorted_suffixes01[ind]] = ind;
			ind++;
		}
		if(sa_triples[i] > f_cnt)
		{
			sorted_suffixes01[ind] = (sa_triples[i]-f_cnt-1)*3 + 1;
			sorted_suffixes_ranks01[sorted_suffixes01[ind]] = ind;
			ind++;
		}
	}
	vector<pp> pairs2;
	for(int i=2; i<text.size(); i += 3)
	{
		if(i+1 >= text.size())
			pairs2.push_back(pp(text[i], 0, i));
		else
			pairs2.push_back(pp(text[i], sorted_suffixes_ranks01[i+1] + 1 , i));
	}

	// cout<<text.size()<<" len\n";
	vector< vector<pp> > bucketspp(charset_size+1);
	for(int i=1; i>=0; --i)
	{
		for(int j=0; j<pairs2.size(); ++j)
		{
			// cout<<pairs2[j].vals[i]<<" "<<charset_size<<"\n";
			bucketspp[pairs2[j].vals[i]].push_back(pairs2[j]);
		}
		// cout<<i<<" i\n";
		int index = 0;
		for(int j=0; j<charset_size + 1	; ++j)
		{
			for(int k=0; k<bucketspp[j].size(); ++k)
				pairs2[index++] = bucketspp[j][k];
			bucketspp[j].clear();
		}
	}
	// cout<<"OK\n";

	vector<unsigned int> sorted_suffixes2(pairs2.size());
	for(int i=0; i<pairs2.size(); ++i)
		sorted_suffixes2[i] = pairs2[i].pos;

	// for(int i=0; i<sorted_suffixes01.size(); ++i)
	// 	cout<<sorted_suffixes01[i]<<" | ";
	// cout<<"\n";
	// for(int i=0; i<text.size(); ++i)
	// 	cout<<sorted_suffixes_ranks01[i]<<" | ";
	// cout<<"\n";

	// for(int i=0; i<sorted_suffixes2.size(); ++i)
	// 	cout<<sorted_suffixes2[i]<<", ";
	// cout<<"\n";

	// return res;
	int i1 = 0;
	int i2 = 0;
	int k = 0;
	while(i1 < sorted_suffixes2.size() && i2 < sorted_suffixes01.size())
	{
		int a = sorted_suffixes2[i1];
		int b = sorted_suffixes01[i2];

		bool done = false;
		if(b % 3 == 1)
		{
			if( text[a] != text[b])
			{
				if(text[a] < text[b])
					res[k++] = sorted_suffixes2[i1++];
				else
					res[k++] = sorted_suffixes01[i2++];
				done = true;
			}
			else
			{
				a++; b++;
				if(a == text.size())
				{
					res[k++] = sorted_suffixes2[i1++];
					done = true;
				}
				if(b == text.size())
				{
					res[k++] = sorted_suffixes01[i2++];
					done = true;
				}
			}
		}
		if(!done)
		{
			int rA = -1, rB = -1;
			if(a + 1 < text.size())
				rA = sorted_suffixes_ranks01[a + 1];
			
			if(b + 1 < text.size())
				rB = sorted_suffixes_ranks01[b + 1];

			if(text[a] < text[b] || (text[a] == text[b] && rA < rB) )
				res[k++] = sorted_suffixes2[i1++];
			else
				res[k++] = sorted_suffixes01[i2++];
		}
	}
	// cout<<"sc "<<" "<<k<<" "<<"\n";
	// cout<<i1<<" "<<sorted_suffixes2.size()<<"\n";
	// cout<<i2<<" "<<sorted_suffixes01.size()<<"\n";

	while(i1 < sorted_suffixes2.size())
		res[k++] = sorted_suffixes2[i1++];
	
	while(i2 < sorted_suffixes01.size())
		res[k++] = sorted_suffixes01[i2++];


	// for(int i=0; i<res.size(); ++i)
	// 	cout<<res[i]<<" ";
	// cout<<" SA\n";
	// vector<unsigned int> sorted_suffixes_ranks01(sub_size);


	return res;

}