all: burrows-wheeler

GCC = g++ -O2 -std=c++11
OPT = -I include

burrows-wheeler: burrows-wheeler.o suffix_tree.o bw_compression.o suffix_array.o
	$(GCC) suffix_tree.o suffix_array.o burrows-wheeler.o bw_compression.o huffman.o $(OPT) -o bin/burrows-wheeler


burrows-wheeler.o: burrows-wheeler.cpp suffix_tree.o bw_compression.o  huffman.o suffix_array.o
	$(GCC) -c burrows-wheeler.cpp $(OPT)

suffix_tree.o: suffix_tree.cpp suffix_tree.h
	$(GCC) -c suffix_tree.cpp $(OPT)

bw_compression.o: bw_compression.cpp bw_compression.h suffix_tree.o huffman.o suffix_array.o
	$(GCC) -c bw_compression.cpp $(OPT)

huffman.o: huffman.cpp huffman.h
	$(GCC) -c huffman.cpp

suffix_array.o: suffix_array.cpp huffman.h
	$(GCC) -c suffix_array.cpp

clean:
	rm -f *.o